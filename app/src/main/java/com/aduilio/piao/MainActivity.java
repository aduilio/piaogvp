package com.aduilio.piao;

import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.aduilio.piao.util.PreferencesUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements Runnable, MediaPlayer.OnCompletionListener {

    private final int SPEED_INITIAL = 450;
    private final int STILL_STRONG = 300;
    private final int LAST_LAP = 150;
    private final int SPEED_TOLERANCE = 5;
    private final int REPEAT_SPEED_CONTROL = 2;
    private final int SPEED_FINAL = 70;

    private ImageView imgPivot;
    private ImageButton ibSilvio;
    private int speed;
    private int speedReference = SPEED_INITIAL + SPEED_TOLERANCE;
    private int repeatSpeed;
    private float volume;
    float oldX = 0;
    float oldY = 0;
    private Date oldDate;
    private Date currentDate;
    private MediaPlayer backgroundPlayer;
    private MediaPlayer silvioPlayer;
    private boolean canRun;
    private Handler mHandler;

    private List<Integer> images;
    private int currentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        mHandler = new Handler();

        imgPivot = findViewById(R.id.ivPiao);
        imgPivot.setBackgroundResource(R.drawable.num1);

        images = new ArrayList<>();
        images.add(R.drawable.num1);
        images.add(R.drawable.num1_);
        images.add(R.drawable.num2);
        images.add(R.drawable.num2_);
        images.add(R.drawable.num3);
        images.add(R.drawable.num3_);
        images.add(R.drawable.num4);
        images.add(R.drawable.num4_);
        images.add(R.drawable.num5);
        images.add(R.drawable.num5_);
        images.add(R.drawable.num6);
        images.add(R.drawable.num6_);

        currentImage = 0;

        ibSilvio = findViewById(R.id.ibSilvio);
        if (PreferencesUtil.getInstance(MainActivity.this).getSilvio()) {
            ibSilvio.setTag("on");
            ibSilvio.setImageResource(R.drawable.silvio_on);

            mHandler.postDelayed(new RunPivot(), 1000);
        } else {
            ibSilvio.setTag("off");
            ibSilvio.setImageResource(R.drawable.silvio_off);
        }
        ibSilvio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ibSilvio.getTag().equals("on")) {
                    PreferencesUtil.getInstance(MainActivity.this).setSilvio(false);
                    ibSilvio.setTag("off");
                    ibSilvio.setImageResource(R.drawable.silvio_off);
                } else {
                    PreferencesUtil.getInstance(MainActivity.this).setSilvio(true);
                    ibSilvio.setTag("on");
                    ibSilvio.setImageResource(R.drawable.silvio_on);
                }
            }
        });

        canRun = true;

        imgPivot.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (canRun) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            oldX = event.getX();
                            oldY = event.getY();
                            oldDate = new Date();
                            break;
                        case MotionEvent.ACTION_UP:
                            float currentX = event.getX();
                            float currentY = event.getY();
                            currentDate = new Date();

                            if (Math.abs(oldY - currentY) < imgPivot.getHeight() / 3
                                    && Math.abs(oldX - currentX) > imgPivot.getWidth() / 3) {

                                if (oldX > currentX) {
                                    backgroundPlayer = MediaPlayer.create(MainActivity.this, R.raw.mus);
                                    volume = 100;
                                    backgroundPlayer.setVolume(1, 1);
                                    backgroundPlayer.start();

                                    // verifica o tempo gasto na ação do toque pra determinar a velocidade inicial
                                    if ((currentDate.getTime() - oldDate.getTime()) < 400) {
                                        speed = SPEED_INITIAL;
                                    } else if ((currentDate.getTime() - oldDate.getTime()) < 600) {
                                        speed = SPEED_INITIAL - 120;
                                    } else {
                                        speed = SPEED_INITIAL - 250;
                                    }
                                    speed += new Random().nextInt(6) * SPEED_TOLERANCE;
                                    repeatSpeed = 0;

                                    // verifica o espaço usado no toque pra definir a duração da animação
                                    if (((oldX - currentX) * 100 / imgPivot.getWidth()) > 70) {
                                        //multiplicador = 2;
                                    } else {
                                        //multiplicador = 1;
                                    }

                                    mHandler.post(MainActivity.this);
                                }
                            }

                            break;
                    }
                }

                return true;
            }
        });

        showInstructions();
    }

    @Override
    public void run() {
        if (speed > SPEED_FINAL || currentImage % 2 != 0) {
            canRun = false;

            setCurrentImage();

            imgPivot.setBackgroundResource(images.get(currentImage));
            mHandler.postDelayed(MainActivity.this, speedReference - speed);

            repeatSpeed++;

            int repeatSpeedTolerance = 0;
            if (speed > SPEED_TOLERANCE + SPEED_FINAL) {
                repeatSpeedTolerance = speed / 100 + 1;
                repeatSpeedTolerance %= REPEAT_SPEED_CONTROL;
            }

            if (repeatSpeed > repeatSpeedTolerance) {
                repeatSpeed = 0;
                speed -= SPEED_TOLERANCE;
            }

            if (speed == STILL_STRONG) {
                mHandler.post(new PivotHasStrength());
            }

            if (speed == LAST_LAP) {
                mHandler.post(new LastLap());
            }

            if (speed < SPEED_FINAL + SPEED_TOLERANCE * 4) {
                mHandler.post(new Volume());
            }
        } else {
            mHandler.postDelayed(new Finalize(), 1000);
            canRun = true;
        }
    }

    private void setCurrentImage() {
        currentImage++;
        if (currentImage > images.size() - 1) {
            currentImage = 0;
        }
    }

    @Override
    protected void onPause() {
        if (mHandler != null) {
            mHandler.removeCallbacks(MainActivity.this);
            mHandler.removeCallbacks(new Volume());
            mHandler.removeCallbacks(new Finalize());
        }

        if (backgroundPlayer != null && backgroundPlayer.isPlaying()) {
            backgroundPlayer.release();
            backgroundPlayer = null;
        }

        if (silvioPlayer != null && silvioPlayer.isPlaying()) {
            silvioPlayer.release();
            silvioPlayer = null;
        }

        canRun = true;

        super.onPause();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        silvioPlayer.release();
        silvioPlayer = null;
    }

    private class Volume implements Runnable {

        @Override
        public void run() {
            if (backgroundPlayer != null && backgroundPlayer.isPlaying()) {
                volume -= 5;
                float v = (float) (1 - (Math.log(100 - volume) / Math.log(100)));
                backgroundPlayer.setVolume(v, v);
            }
        }
    }

    private class RunPivot implements Runnable {

        @Override
        public void run() {
            if (PreferencesUtil.getInstance(MainActivity.this).getSilvio() && silvioPlayer == null) {
                silvioPlayer = MediaPlayer.create(MainActivity.this, R.raw.run_pivot);
                silvioPlayer.setVolume(0.9f, 0.9f);
                silvioPlayer.setOnCompletionListener(MainActivity.this);
                silvioPlayer.start();
            }
        }
    }

    private class PivotHasStrength implements Runnable {

        @Override
        public void run() {
            if (PreferencesUtil.getInstance(MainActivity.this).getSilvio() && silvioPlayer == null) {
                silvioPlayer = MediaPlayer.create(MainActivity.this, R.raw.pivot_has_streength);
                silvioPlayer.setVolume(0.9f, 0.9f);
                silvioPlayer.setOnCompletionListener(MainActivity.this);
                silvioPlayer.start();
            }
        }
    }

    private class LastLap implements Runnable {

        @Override
        public void run() {
            if (PreferencesUtil.getInstance(MainActivity.this).getSilvio() && silvioPlayer == null) {
                silvioPlayer = MediaPlayer.create(MainActivity.this, R.raw.last_lap);
                silvioPlayer.setVolume(0.9f, 0.9f);
                silvioPlayer.setOnCompletionListener(MainActivity.this);
                silvioPlayer.start();
            }
        }
    }

    private class Finalize implements Runnable {

        @Override
        public void run() {
            canRun = true;

            if (backgroundPlayer != null && backgroundPlayer.isPlaying()) {
                backgroundPlayer.stop();
            }
        }
    }

    private void showInstructions() {
        boolean showInfo = PreferencesUtil.getInstance(MainActivity.this).getShowInfo();

        if (showInfo) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.info))
                    .setNegativeButton(R.string.dont_ask,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    PreferencesUtil.getInstance(MainActivity.this).setShowInfo(false);
                                }
                            })
                    .setNeutralButton("OK", null)
                    .create().show();
        }
    }
}
