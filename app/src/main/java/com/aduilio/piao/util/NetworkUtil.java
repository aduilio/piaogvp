package com.aduilio.piao.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
	private ConnectivityManager connectivityManager;
	private static NetworkUtil instance = null;

	public static NetworkUtil getInstance() {
		if (instance == null) {
			instance = new NetworkUtil();
		}

		return instance;
	}

	private NetworkUtil() {

	}

	public boolean isConnected(Context context) {
		connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}

		return false;
	}
}
