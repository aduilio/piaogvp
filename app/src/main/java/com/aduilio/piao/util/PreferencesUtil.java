package com.aduilio.piao.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesUtil {

    private final String RATE_QTT = "rateqtt";
    private final String INTERSTITIAL = "interstitial";
    private final String SHOW_INFO = "showInfo";
    private final String YES_NO = "yesno";
    private final String SILVIO = "silvio";

    private static PreferencesUtil instance;
    SharedPreferences preferences;

    private PreferencesUtil(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static PreferencesUtil getInstance(Context context) {
        if (instance == null) {
            instance = new PreferencesUtil(context);
        }

        return instance;
    }

    public void setShowInfo(boolean showInfo) {
        preferences.edit().putBoolean(SHOW_INFO, showInfo).apply();
    }

    public boolean getShowInfo() {
        return preferences.getBoolean(SHOW_INFO, true);
    }

    public void setYesNo(boolean yesno) {
        preferences.edit().putBoolean(YES_NO, yesno).apply();
    }

    public boolean getYesNo() {
        return preferences.getBoolean(YES_NO, true);
    }

    public void setSilvio(boolean silvio) {
        preferences.edit().putBoolean(SILVIO, silvio).apply();
    }

    public boolean getSilvio() {
        return preferences.getBoolean(SILVIO, true);
    }

}
